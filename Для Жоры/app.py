import joblib
import numpy as np
import pandas as pd
from flask import Flask, request, render_template
from model import model


app = Flask(__name__)


def get_prediction(X_test):
    prediction = model.predict(X_test)
    return str(prediction)



@app.route('/', methods=['post', 'get'])
def login():
    message = ''
    if request.method == 'POST':

        var3 = request.form.get('недельное удержание')

        X_test = pd.DataFrame([[var3]])
        message = 'Затраты на рекламу рекомендую:', get_prediction(X_test)

    return render_template('login.html', message=message)


if __name__ == '__main__':


    app.run()
